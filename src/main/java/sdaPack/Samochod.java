package sdaPack;

public class Samochod extends Pojazd {

    private String marka;
    private int ileLiterA;
    final private static int MAX_PREDKOSC = 220;
    final private static int MIN_PREDKOSC = -40;
    final private static int PREDKOSC_POZIOM_2 = 100;

    public Samochod(String marka) {
        predkosc = 0;
        this.marka = marka;
        ileLiterA = policzIleLiter('a');
    }

    @Override
    public void jedz() {
        System.out.println("Samochod jedzie");
    }

    @Override
    public void stoj() {
        System.out.println("Samochod stoi");
    }

    @Override
    public void trab() {
        System.out.println("Samochod trabi");
    }

    @Override
    public void przyspiesz() {
        double aktualnaPredkosc = getPredkosc();
        if (aktualnaPredkosc < MAX_PREDKOSC) {
            if (aktualnaPredkosc >= PREDKOSC_POZIOM_2) {
                aktualnaPredkosc = aktualnaPredkosc + 2 + 2 * ileLiterA;
            } else {
                aktualnaPredkosc += 2;
            }
            if (aktualnaPredkosc > MAX_PREDKOSC) {
                aktualnaPredkosc = MAX_PREDKOSC;
            }
            System.out.println("Samochód przyspiesza");
            predkosc = aktualnaPredkosc;
        }
    }


    @Override
    public void zwolnij() {
        double aktualnaPredkosc = getPredkosc();
        if (aktualnaPredkosc <= MIN_PREDKOSC) {
            throw new IllegalSpeedException();
        }
        aktualnaPredkosc -= 5;
        if (aktualnaPredkosc < MIN_PREDKOSC) {
            aktualnaPredkosc = MIN_PREDKOSC;
        }
        predkosc = aktualnaPredkosc;
        System.out.println("Samochód zwalnia");
    }


    private int policzIleLiter(char szukanaLitera) {
        int ileLiter = 0;
        char szukanaLiteraMala = Character.toLowerCase(szukanaLitera);
        char szukanaLiteraDuza = Character.toUpperCase(szukanaLitera);

        for (int i = 0; i < marka.length(); i++) {
            if (marka.charAt(i) == szukanaLiteraDuza || marka.charAt(i) == szukanaLiteraMala) {
                ileLiter++;
            }
        }
        return ileLiter;
    }
}
