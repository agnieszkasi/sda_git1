package sdaPack;

import javax.print.attribute.standard.PresentationDirection;
import java.util.ArrayList;
import java.util.Optional;

public class Tunel {
    private ArrayList<Pojazd> pojazdy;

    public Tunel() {
        pojazdy = new ArrayList<Pojazd>();
    }

    public int ilePojazdow() {
        return pojazdy.size();
    }

    public void dodajPojazd(Pojazd pojazd) {
        pojazdy.add(pojazd);
    }

    public double sredniaPredkosciW_Tunelu() {
        double suma = 0;
        for (Pojazd el : pojazdy) {
            suma += el.getPredkosc();
        }
        return suma / pojazdy.size();
    }

    public Optional<Pojazd> pojazdMinPredkosc() {
        if (pojazdy.size() == 0) {
            return Optional.empty();
        }
        double minPredkosc = pojazdy.get(0).getPredkosc();
        int index = 0;
        for (int i = 1; i < pojazdy.size(); i++) {
            if (pojazdy.get(i).getPredkosc() < minPredkosc) {
                minPredkosc = pojazdy.get(i).getPredkosc();
                index = i;
            }
        }
        return Optional.of(pojazdy.get(index)) ;
    }


    public Optional<Pojazd> pojazdMaxPredkosc() {

        if (pojazdy.size() == 0) {
            return Optional.empty();
        }
        double maxPredkosc = pojazdy.get(0).getPredkosc();
        int index = 0;
        for (int i = 1; i < pojazdy.size(); i++) {
            if (pojazdy.get(i).getPredkosc() > maxPredkosc) {
                maxPredkosc = pojazdy.get(i).getPredkosc();
                index = i;
            }
        }
        return Optional.of(pojazdy.get(index));
    }
}


