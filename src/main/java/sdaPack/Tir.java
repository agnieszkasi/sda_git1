package sdaPack;

public class Tir extends Pojazd {
    private String marka;
    private int numerZatrabienia;
    final private static int MAX_PREDKOSC = 80;
    final private static int MIN_PREDKOSC = -10;

    public Tir(String marka) {
        numerZatrabienia = 0;
        this.marka = marka;
        predkosc = 0;
    }

    @Override
    public void jedz() {

        System.out.println("Tir jedzie");
    }

    @Override
    public void stoj() {
        System.out.println("Tir stoi");
    }

    @Override
    public void przyspiesz() {
        double aktualnaPredkosc = getPredkosc();
        if (aktualnaPredkosc >= MAX_PREDKOSC) {
            return;
        }
        aktualnaPredkosc += 1;
        predkosc = aktualnaPredkosc;
        System.out.println("Tir przyspiesza");
    }

    @Override
    public void zwolnij() {
        double aktualnaPredkosc = getPredkosc();
        if (aktualnaPredkosc <= MIN_PREDKOSC) {
            return;
        }
        aktualnaPredkosc = aktualnaPredkosc - 3 - 3 * numerZatrabienia;
        if (aktualnaPredkosc < MIN_PREDKOSC) {
            aktualnaPredkosc = MIN_PREDKOSC;
        }
        predkosc = aktualnaPredkosc;
        System.out.println("Tir zwalnia");
    }

    @Override
    public void trab() {
        System.out.println("Tir trąbi");
        if (numerZatrabienia < 3) {
            numerZatrabienia++;
        }
    }

}
